﻿// See https://aka.ms/new-console-template for more information

using FaineTech.Console;
using McMaster.Extensions.CommandLineUtils;

var app = new CommandLineApplication();
app.HelpOption();

var nameOfFunction = app.Argument("nameOfFunction", "For Fibonacci sequence please type: fib or for Ferma sequence type: fer ")
    .IsRequired();
var numberOfSequence = app.Argument<int>("numberOfSequence", "Please enter the required number to calculate the sequence")
    .IsRequired();

var outputType = app.Option("-o|--output <TYPE>", "Types for output: console or file", CommandOptionType.MultipleValue);
outputType.DefaultValue = "console";   

var pathFile = app.Option("--file <path>", "Please type existing path to create file with sequence", CommandOptionType.SingleValue);

app.OnExecute(() =>
{
    ICommand command = CommandFactory.CreateCommand(nameOfFunction.Value, numberOfSequence.ParsedValue);
                
    if (command == null)
    {
        app.ShowHelp();
        return;
    } 
    
    double[] sequence = command.Execute();
       
    
    string pathOfFile = pathFile.Value();

    if (!string.IsNullOrWhiteSpace(pathOfFile) && !Directory.Exists(pathOfFile))
    {
        app.ShowHelp();
        return;
    }

    IOutput[] output = OutputFactory.CreateOutput(outputType.Values, pathOfFile);
            
    for (int i = 0; i < output.Length; i++)
    {
        output[i].Write(sequence);
    }      
    
});

try
{
    return app.Execute(args);
} catch(Exception ex)
{
    Console.WriteLine(ex.Message);
    app.ShowHelp();
    return 1;
}

