﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FaineTech.Console
{
    internal static class CommandFactory
    {
        public static ICommand CreateCommand(string nameOfFunction, int numberOfSequence)
        {
            if (nameOfFunction == "fib" & numberOfSequence >= 1)
            {
                return new FibonacciCommand(numberOfSequence); 
            }
            if (nameOfFunction == "fer")
            {
                return new FermaCommand(numberOfSequence);
            }
            return null;            
        }
    }
}
