﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FaineTech.Console
{
    internal static class OutputFactory
    {
        public static IOutput[] CreateOutput(IReadOnlyList<string?> destination, string pathOfFile)
        {   
            IOutput[] output = new IOutput[destination.Count];

            foreach (string outputDestenation in destination)
            {
                if (outputDestenation == "console")
                {
                    output[0] = new ConsoleOutput();
                }
                if (outputDestenation == "file")
                {
                    output[1] = new FileOutput(pathOfFile);
                }                           
            }
            return output;
        }                
    }
}
