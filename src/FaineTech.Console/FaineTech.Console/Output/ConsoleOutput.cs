﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FaineTech.Console
{
    internal class ConsoleOutput: IOutput
    {
                
        public void Write(double[] sequence)
        {     
                    
            foreach (double number in sequence)
            {
                System.Console.WriteLine(number);
            }
        }        
        
    }
}
