﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FaineTech.Console
{
    internal class FileOutput: IOutput
    {
        private string pathOfFile;

        public FileOutput(string pathOfFile)
        {
            this.pathOfFile = pathOfFile;
        }

        public void Write(double[] sequence)
        {
                        
            if (pathOfFile == null)
            {
                using StreamWriter file = new("Sequence.txt");
                foreach (double number in sequence)
                {
                    file.WriteLine(number);
                }
            }
            else  
            {                
                using StreamWriter file = new StreamWriter(Path.Combine(pathOfFile, "Sequence.txt"));
                foreach (double number in sequence)
                {
                    file.WriteLine(number);
                }
            }  
            
        }
    }
}
