﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FaineTech.Console
{
    internal class FermaCommand: ICommand
    {        
        private int numberOfSequence;

        public FermaCommand(int numberOfSequence)
        {            
            this.numberOfSequence = numberOfSequence;
        }

        public double[] Execute()
        {            
            double a1 = 2, a2 = 1;
            double a3 = a1 * a2;
            double[] arrayFerma = new double[numberOfSequence];

            for (int i = 1; i <= numberOfSequence; i++)
            {
                arrayFerma[i - 1] = a3 + 1;
                a1 = a3;
                a2 = a3;
                a3 = a1 * a2;
            }
            return arrayFerma;
        }                
    }
}
