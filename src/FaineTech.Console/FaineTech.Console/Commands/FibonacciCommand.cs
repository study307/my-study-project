﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FaineTech.Console
{
    internal class FibonacciCommand: ICommand
    {        
        private int numberOfSequence;

        public FibonacciCommand(int numberOfSequence)
        {            
            this.numberOfSequence = numberOfSequence;
        }
         
        public double[] Execute()
        {            
            {
                double a1 = 0, a2 = 1;
                double a3 = a1 + a2;
                double[] arrayFibonacci = new double[numberOfSequence];
                arrayFibonacci[0] = a1;
                arrayFibonacci[1] = a2;

                for (int i = 3; i <= numberOfSequence; i++)
                {
                    arrayFibonacci[i - 1] = a3;
                    a1 = a2;
                    a2 = a3;
                    a3 = a1 + a2;
                }
                return arrayFibonacci;
            }            
        }
    }
}
