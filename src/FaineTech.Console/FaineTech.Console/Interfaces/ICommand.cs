﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FaineTech.Console
{
    internal interface ICommand
    {
        double[] Execute();
    }
}
